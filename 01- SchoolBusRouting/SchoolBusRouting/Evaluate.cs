﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VRP
{
    public class clsEvalSolution
    {
        /*--------------------------------------------------------------------------------------------*/
        public void Evaluate(double[,] Cost, List<sRequests> Requests, List<sNodes> Nodes, 
            List<sRoutes> Routes, out double Travel, out double Window, out double Shift)
        // travel times, time window violation and shift violation of a given solution is calculated
        {
            // initial values
            Travel = 0; // travel times
            Window = 0; // time window violations
            Shift = 0; // shift violation            

            // function variables
            int node1, node2, node3, route;
            List<sFromTo> Seq = new List<sFromTo>();
            List<sOrigDest> Pick = new List<sOrigDest>();

            // Main evaluation loop
            for (route = 0; route < Routes.Count; route++)
            {
                // current sequence and pick-up times                
                Seq = Routes[route].seq.ToList();
                Pick = Routes[route].pickup.ToList();

                // the rest of the nodes in the tour    
                for (int index = 0; index < Seq.Count - 1; index++)
                {
                    if (index == 0)
                    {
                        node1 = Routes[route].Depot; // depot 
                        node2 = Seq[0].From; // request
                        node3 = Seq[0].To; // centre 
                    }
                    else
                    {
                        node1 = Seq[index - 1].From; // request origin node
                        node2 = Seq[index - 1].To; // request destination node
                        node3 = Seq[index].From; // next request origin node or break start or depot
                    }
                        
                    // time window violations
                    Window += Math.Max(0, Pick[index - 1].Origin - Requests[node1].LatePickUp);
                    Window += Math.Max(0, Pick[index - 1].Destin - Requests[node2].LateDelivery);

                    // travel times
                    Travel += Cost[node1, node2] + Cost[node2, node3];
                }
                Travel = Seq.Count > 0 ? Travel + Cost[Seq[Seq.Count-1].To, Routes[route].Depot]: Travel;
                 
                // calculate shift violation
                Shift = Pick.Count > 0 ? Shift + Math.Max(0, Pick[Pick.Count - 1].Destin - Routes[route].FinishTime): Shift;                
            }
        }
        /*--------------------------------------------------------------------------------------------*/
        public void RouteCost(List<int> Seq, List<double> Pick, double[,] Cost, List<sRequests> Requests, 
            List<sNodes> Nodes, List<sRoutes> Routes, int route, out double Travel, out double Window, out double Shift)
        // A given route is evaluated
        {
            // initial values
            Travel = 0; // travel times
            Window = 0; // time window violations
            Shift = 0; // shift violation

            // function variables
            int index = 1;
            int node1 = Seq[0];  // depot
            int node2 = Seq[1]; // request or start of a break
            int node3 = Seq[2]; // centre or end of a break            
            Boolean Found;
            int nodeCode1 = -1, nodeCode2 = -1, nodeCode3 = -1;

            // distance from the depot to the fist node
            nodeCode1 = node1;
            Found = false;
            while (!Found)
            {
                node2 = Seq[index];
                if (node2 >= 0)
                {
                    Found = true;
                    if (index == Seq.Count - 1)
                        nodeCode2 = node2; // a depot
                    else
                        nodeCode2 = node2; // a request node
                    index = index + 1;
                }
                index += 2;
            }
            Travel = Travel + Cost[nodeCode1, nodeCode2];
            index -= 2;
            // the rest of the nodes in the tour    
            while (index < Seq.Count - 1)
            {
                node1 = Seq[index - 1]; // request origin node or break start
                nodeCode1 = node1;
                node2 = Seq[index]; // request destination node
                nodeCode2 = node2;
                node3 = Seq[index + 1]; // next request origin node or break start or depot
                // time window violations
                Window += Math.Max(0, Pick[index - 1] - Requests[node2].LatePickUp);
                Window += Math.Max(0, Pick[index] - Requests[node2].LateDelivery);

                if (node3 < 0) // the node is start of a break rather than a request
                {
                    index = index + 2;
                    Found = false;
                    while (!Found)
                    {
                        node3 = Seq[index + 1];
                        if (node3 >= 0)
                        {
                            Found = true;
                            nodeCode3 = node3;
                        }
                        else
                            index = index + 2;
                    }
                }
                index = index + 2;
                // travel times
                Travel = Travel + Cost[nodeCode1, nodeCode2] + Cost[nodeCode2, nodeCode3];
            }
            // calculate shift violation
            Shift = Math.Max(0, Pick[Pick.Count - 1] - Routes[route].FinishTime);
        }
        /*--------------------------------------------------------------------------------------------*/
        public double TWViolation(List<int> Seq, List<double> Pick, List<sRequests> Request)
        // time window violation of a given route is calculated
        {
            double value = 0;
            int node;
            for (int index = 1; index < Seq.Count - 1; index += 2)
            {
                node = Seq[index];
                if (node > 0)
                {
                    value += Math.Max(Pick[index] - Request[node].LatePickUp, 0);
                    value += Math.Max(Pick[index + 1] - Request[node].LateDelivery,0);
                }
            }
            return value;
        }
        /*--------------------------------------------------------------------------------------------*/
        public double ShiftViolation(List<double> Pick, List<sRoutes> routes, int route)
        // vehicle shift violation of a given route
        {
            double value = Math.Max(Pick[Pick.Count - 1] - routes[route].FinishTime, 0);
            return value;
        }
    }
}
