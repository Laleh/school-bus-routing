﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VRP
{
    public class clsNeighbour
    {
        public void Neighbour(List<sRequests> Requests, List<sRoutes> Solution, List<sNodes> Nodes, 
            double[,] Cost, double Clock, List<sTabu> TabuList, double dAlpha, double dBeta, double Current, List<sRho> Rho,
            out int BNode, out int BRoute, out int BPre, out int BSpot, out List<sFromTo> BS_0, out List<sOrigDest> BP_0, 
            out List<sFromTo> BS_1, out List<sOrigDest> BP_1, out double BTravel1, out double BTravel2, out double BShift1, 
            out double BShift2, out double BWindow1, out double BWindow2, out double R1, out double R2)
        { 
            // function variables
            int node, centre;
            Boolean Tabu = true, Found;
            double Travel1, Window1, Shift1, Travel2, Window2, Shift2, delay, shift, travel, R=0, TGain = 0, WGain = 0, 
                SGain = 0, TempT = 0, TempS = 0, TempW = 0, TempCost = 0, BestCost = Double.PositiveInfinity, TW, Shift, Lambda = 0.015;

            clsEvalRoute Sol = new clsEvalRoute();
            clsUpdates Sol1 = new clsUpdates();
            clsInsertion Sol2 = new clsInsertion();

            // initial values
            BNode = -1; BRoute= -1; BPre= -1; BSpot= -1;
            BTravel1 = 0; BTravel2 = 0; BShift1 = 0; BShift2 = 0; BWindow1 = 0; BWindow2 = 0;
            List<sFromTo> Seq1 = new List<sFromTo>(), Seq2 = new List<sFromTo>();
            List<sOrigDest> Pick1 = new List<sOrigDest>(), Pick2 = new List<sOrigDest>(), NewPick = new List<sOrigDest>();
            BS_0 = Seq1.ToList(); BS_1 = Seq1.ToList(); BP_0 = Pick1.ToList(); BP_1 = Pick1.ToList(); R1 = 0; R2 = 0;

            // Main Neighbour loop
            for (int route0 = 0; route0 < Solution.Count; route0++) // search all original routes/vehicles
            {
                for (int index = 0; index < Solution[route0].seq.Count; index++) // insert after each depot/centre
                {
                    node = Solution[route0].seq[index].From; // the node to be replaced
                    if (Solution[route0].pickup[index].Origin > Clock && node > 0)                    
                    {                        
                        centre = Requests[node].Destin;  // node's destination point
                        // revise the old sequence and pick-up times
                        Seq1 = Solution[route0].seq.ToList(); // new sequence
                        Seq1.RemoveAt(index);  // remove node and its centre
                        Pick1 = Solution[route0].pickup.ToList();
                        Pick1.RemoveAt(index);  // remove arrival time at nodeand its centre
                        Sol1.UpdatePick(Requests, Solution, Nodes, route0, Seq1, Pick1, Cost, index, 
                            out NewPick, out delay, out travel, out shift); // new pick-up
                        // calculate "gains" by removing node from route0
                        WGain = Solution[route0].WVal - delay; // original TW violation - new    
                        TGain = Solution[route0].TVal - travel; // original travel time - new 
                        SGain = Solution[route0].SVal - shift; // original shift violation - new

                        // write pre changes here???????????????
                        for (int route1 = 0; route1 < Solution.Count; route1++)
                        {
                            Tabu = false; // check  if the move of node from route0 to route1 is tabu
                            for (int T = 0; T < TabuList.Count; T++)
                            {
                                if (node == TabuList[T].Node && route1 == TabuList[T].Route)
                                {
                                    Tabu = true;
                                    break;
                                }
                            }
                            if (!Tabu && route1 != route0)
                            {
                                // insert node in route1 where it best fits                                    
                                Sol2.BestInsertion(Solution, Requests, Nodes, route1, node, 
                                    Cost, Clock, out Seq2, out Pick2, out TW, out Shift, out Found);

                                TempCost = 0; TempS = 0; TempT = 0; TempW = 0;
                                // evaluate the solution
                                Sol.EvalRoute(Cost, Requests, Nodes, Solution, Seq1, Pick1, route0, 
                                    out Travel1, out Window1, out Shift1);
                                Sol.EvalRoute(Cost, Requests, Nodes, Solution, Seq2, Pick2, route1,
                                    out Travel2, out Window2, out Shift2);
                                TempT += Travel1 + Travel2;
                                TempS += Shift1 + Shift2;
                                TempW += Window1 + Window2;
                                for (int rr = 0; rr < Solution.Count && rr != route0 && rr != route1; rr++)
                                {
                                    TempT += Solution[rr].TVal;
                                    TempS += Solution[rr].SVal;
                                    TempW += Solution[rr].WVal;
                                }
                                //TempCost = TempT + dAlpha * TempW + dBeta * TempS;
                                TempCost = TempT + TempW + TempS;

                                // compare the solution with the current solution
                                if (TempCost >= Current)
                                {
                                    // add P(s) to the solution to diversify
                                    R = Solution[route1].RVal - Rho[node].Route[route0] + Rho[node].Route[route1];
                                    TempCost += Lambda * TempT * Math.Sqrt((Requests.Count - 1) * (Solution.Count - 1)) * R;
                                }

                                // compare the solution with other neighbours
                                if (TempCost < BestCost)
                                {
                                    BestCost = TempCost;
                                    BRoute = route1;
                                    BPre = route0;
                                    BNode = node;
                                    BS_0 = Seq1;
                                    BS_1 = Seq2;
                                    BP_0 = Pick1;
                                    BP_1 = Pick2;
                                    BTravel1 = Travel1; BTravel2 = Travel2;
                                    BShift1 = Shift1; BShift2 = Shift2;
                                    BWindow1 = Window1; BWindow2 = Window2;
                                    R1 = Solution[route0].RVal - Rho[node].Route[route0];
                                    R2 = Solution[route1].RVal + Rho[node].Route[route1];
                                } // if (TempCost < BestCost)
                            }  // if (!Tabu)                    
                        } // for (int route1 = 0; route1 < Solution.Count && route1 != route0; route1++)
                    } // if (Solution[route0].pickup[index] > Clock && node > 0)  
                } // for (int index = 1; index < Solution[route0].seq.Count - 1; index += 2) 
            } // for (int route0 = 0; route0 < Solution.Count; route0++)
        } // public void Neighbour
    }
}
