﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Windows.Forms;

namespace VRP
{
    public class clsSavings
    {
        [STAThread]
        public void Savings(List<sRequests> Requests, List<sRoutes> Routes, List<sNodes> Nodes, 
            double[,] Cost, double Alpha, double Beta, out List<sRoutes> Solution)
        {
            clsInsertion solutin = new clsInsertion();
            // function variables
            int node;
            Boolean InsFound;
            double BestCost, TmpCost, TTW, TShift, Clock = 0;
            int BRoute = -1;
            List<sFromTo> BSeq = new List<sFromTo>(), seq = new List<sFromTo>();
            List<sOrigDest> BPick = new List<sOrigDest>(), pick = new List<sOrigDest>();           

            // compile a list of nodes to be inserted into vehicle routes
            List<int> unused = new List<int>();
            for (int index = 0; index < Requests.Count; index++)
            {
                unused.Add(index);
            }

            // Main initilaization loop
            while (unused.Count > 0)
            {
                // best insertion cost so far
                BestCost = double.PositiveInfinity;
                // select a random unused node 
                Random rnd = new Random();
                int row = rnd.Next(unused.Count);
                node = unused[row];
                // search through all the routes for inserting the node
                for (int route = 0; route < Routes.Count; route++ )
                {
                    // find insertion cost for node in the route
                    solutin.BestInsertion(Routes, Requests, Nodes, route, node, Cost, Clock,
                        out seq, out pick, out TTW, out TShift, out InsFound);
                    TmpCost = Alpha * TTW + Beta * TShift;
                    if (TmpCost < BestCost && InsFound)
                    {
                        BestCost = TmpCost;
                        BRoute = route;
                        BSeq = seq;
                        BPick = pick;            
                        if (TTW == 0 && TShift == 0)
                            break;
                    }
                }
                // update solution
                Routes[BRoute].seq = BSeq.ToList();
                Routes[BRoute].pickup = BPick.ToList();
                // remove node from the list of unused (uninserted) nodes
                unused.RemoveAt(row);
            }
            Solution = Routes.ToList();
        }
    }
}
