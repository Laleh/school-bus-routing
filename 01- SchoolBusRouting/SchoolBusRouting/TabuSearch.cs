﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VRP
{
    public class clsTabuSearch
    {
        public void UnifiedTS(List<sRoutes> Routes,List<sRequests> Requests, List<sNodes> Nodes, 
            double[,] Cost, double Clock, out List<sRoutes> BSolution, out List<sCostLog> CostLog)
        {
            CostLog = new List<sCostLog>();
            // function variables
            List<sRoutes> TSolution = new List<sRoutes>(); // current solution
            //List<sRoutes> BSolution = new List<sRoutes>(); // best solution so far
            TSolution = Routes.ToList();
            BSolution = Routes.ToList();
            double TCost = 0, BCost, Current; // temp, best and current costs           
            double travel = 0, window = 0, shift = 0, BTravel1, BTravel2, BShift1, BShift2, BWindow1, BWindow2, R1, R2;
            int BNode, BRoute, BPre, BSpot;
            List<sFromTo> BS_0, BS_1 = new List<sFromTo>();
            List<sOrigDest> BP_0, BP_1 = new List<sOrigDest>();

            clsEvalSolution Sol = new clsEvalSolution();
            clsNeighbour Sol1 = new clsNeighbour();

            // Tabu search parameters
            int iterations = 100;             
            List<sRho> Rho = new List<sRho>();
            for (int request = 0; request< Requests.Count; request++)
            {
                Rho.Add(new sRho(new List<int>()));
                for (int route = 0; route < Routes.Count; route++)
                    Rho[request].Route.Add(0);
            
            }
            double fAlpha = 1, fBeta = 1, Delta = 0.5 ; // time winfow and shift violation parameters, fixed parameters
            double dAlpha = 1, dBeta = 1; // dynamic parameters
            int Teta = Convert.ToInt32(Math.Round(7.5 * Math.Log(Requests.Count)));
            List<sTabu> TabuList = new List<sTabu>();
            for (int tb = 0; tb < Teta; tb++)
                TabuList.Add(new sTabu(-1, -1));

            // Evaluate the current solution
            Sol.Evaluate(Cost, Requests, Nodes, Routes, out travel, out window, out shift);
            BCost = travel + fAlpha * window + fBeta * shift;
            Current = BCost;
            CostLog.Add(new sCostLog(window,shift,BCost,BCost));

            // Main Tabu Loop
            for (int iter = 1; iter <= iterations; iter++)
            { 
                // find the best neighbour of a given solution
                Sol1.Neighbour(Requests, TSolution, Nodes, Cost, Clock, TabuList, dAlpha, dBeta, Current, Rho,
                    out BNode, out BRoute, out BPre, out BSpot, out BS_0, out BP_0, out BS_1, out BP_1, out BTravel1, 
                    out BTravel2, out BShift1, out BShift2, out BWindow1, out BWindow2, out R1, out R2);

                if (BNode > 0)
                {
                    // Update solution
                    TSolution = UpdateSolution(TSolution, BPre, BRoute, BS_0, BS_1, BP_0, BP_1, BTravel1, BShift1,  
                        BWindow1, R1, BTravel2, BShift2, BWindow2, R2);

                    // Update the Rho matrix
                    Rho[BNode].Route[BRoute]++;

                    // evaluate the solution                    
                    Sol.Evaluate(Cost, Requests, Nodes, TSolution, out travel, out window, out shift);
                    TCost = travel + fAlpha * window + fBeta * shift;
                    CostLog.Add(new sCostLog(window,shift,Math.Min(BCost,TCost),TCost));

                    // compare the solution against the best solution
                    if (TCost < BCost)
                    {
                        BCost = TCost;
                        BSolution = TSolution.ToList(); // replace the solutions
                        Current = BCost;
                        TabuList.Add(new sTabu(BNode, BRoute));
                    }
                    else
                    {
                        TabuList.Add(new sTabu(-1, -1));
                    }

                    // update parameters
                    UpdateParam(dAlpha, dBeta, Delta, window, shift, out dAlpha, out dBeta);

                    // remove the first item the tabu list 
                    TabuList.RemoveAt(0);                    
                }
            }
        }

        public void UpdateParam(double a, double b, double D, double window, double shift, out double A, out double B)
            // update tabu search parameters
        {
            A = a;
            B = b;

            if (window > 0.1 && a < 100)
                A = a * (1 + D);
            if (window <= 0.05 && a > 0.005)
                A = a / (1 + D);

            if (shift > 0.1 && b < 100)
                B = b * (1 + D);
            if (shift <= 0.05 && b > 0.005)
                B = b / (1 + D);
        }

        public List<sRoutes> UpdateSolution(List<sRoutes> Solution, int BPre, int BRoute, List<sFromTo> BS_0, List<sFromTo> BS_1,
            List<sOrigDest> BP_0, List<sOrigDest> BP_1, double BTravel1, double BShift1, double BWindow1, double R1, double BTravel2, 
            double BShift2, double BWindow2, double R2)
        {
            // update the current solution (replace with the previous best neighbour)
            Solution[BPre].seq = BS_0;
            Solution[BRoute].seq = BS_1;
            Solution[BPre].pickup = BP_0;
            Solution[BRoute].pickup = BP_1;

            // Update Solution evaluation values
            Solution[BPre].TVal = BTravel1;
            Solution[BPre].SVal = BShift1;
            Solution[BPre].WVal = BWindow1;
            Solution[BPre].RVal = R1;
            Solution[BRoute].TVal = BTravel2;
            Solution[BRoute].SVal = BShift2;
            Solution[BRoute].WVal = BWindow2;
            Solution[BRoute].RVal = R2;

            return Solution;
        }
    }
}
