﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VRP
{
    public class clsInitialize
    {
        // the cost matrix
        public void GenerateCost(int n, out double[,] Cost)
        {
            Cost = new double[n, n];
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                for (int j = i + 1; j < n; j++)
                {                    
                    Cost[i, j] = rnd.Next(5);
                    Cost[j, i] = Cost[i, j];
                }
            }
        }
    }
}
