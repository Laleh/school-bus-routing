﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VRP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            clsHeuristic S = new clsHeuristic();
            List<sCostLog> CostLog;
            List<sNodes> Nodes;
            S.MainHeuristic(out CostLog, out Nodes);

            for (int i = 0; i < CostLog.Count; i++)
            {
                chart1.Series["WaitTimes"].Points.AddXY(i, CostLog[i].WVal);
                //chart1.Series["IdleTimes"].Points.AddXY(i, CostLog[i]..BestI);
                chart1.Series["OverTimes"].Points.AddXY(i, CostLog[i].SVal);
                chart1.Series["BestSolution"].Points.AddXY(i, CostLog[i].Best);
                chart1.Series["Solution"].Points.AddXY(i, CostLog[i].Current);
            }
        }

        private void chart1_Click(object sender, EventArgs e) { }

    }
}
