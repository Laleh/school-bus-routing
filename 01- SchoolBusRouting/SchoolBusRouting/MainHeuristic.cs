﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VRP
{
    public class clsHeuristic
    {
        public void MainHeuristic(out List<sCostLog> CostLog, out List<sNodes> Nodes)
        {
            //Application.Run(new Form2());
            clsSavings S2 = new clsSavings();
            clsTabuSearch S3 = new clsTabuSearch();
            clsLoadInput S5 = new clsLoadInput();
            clsInitialize S = new clsInitialize();

            // structures            
            Nodes = new List<sNodes>();
            CostLog = new List<sCostLog>();            
            List<sRequests> Requests = new List<sRequests>();
            List<sRoutes> Routes = new List<sRoutes>();
            double[,] Cost; 

            Console.WriteLine("Generating random travel times...");

            // read input files
            Console.WriteLine("Reading input files...");
            S5.ReadInput(out Nodes, out Requests, out Routes);

            // Generate a random cost matrix
            S.GenerateCost(Nodes.Count, out Cost);

            // Generate an initial solution
            Console.WriteLine("Generataing an initial solution...");
            List<sRoutes> Solution, Solution1, Solution2 = new List<sRoutes>();
            double Alpha = 0.5, Beta = 0.5;
            S2.Savings(Requests, Routes, Nodes, Cost, Alpha, Beta, out Solution1);

            // Unified tabu search: improve the initial solution
            Console.WriteLine("Improving the initial solution...");
            double Clock = 0;
            S3.UnifiedTS(Solution1, Requests, Nodes, Cost, Clock, out Solution, out CostLog);

            // Dynamic module: insert all the emergent calls into the schedule, 
            //S4.Dynamic(vRoutes, A_Requests, E_Requests, vCentres, vDepots, Cost, Clock, 1, 1, out Solution2);

            Console.Write("Done!");
            Console.Read();
        }
    }
}
