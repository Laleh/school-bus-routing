﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VRP
{
    public class clsEvalRoute
    {
        public void EvalRoute(double[,] Cost, List<sRequests> Requests, List<sNodes> Nodes, 
            List<sRoutes> Routes, List<sFromTo> Seq,  List<sOrigDest> Pick, int route, out double Travel, 
            out double Window, out double Shift)
        // travel times, time window violation and shift violation of a given route is calculated
        {
            // initial values
            Travel = 0; // travel times
            Window = 0; // time window violations
            Shift = 0; // shift violation       

            if (Seq.Count == 0)
                return;

            // function variables
            int index = 1, node1, node2, node3;

            // Main evaluation loop
            node1 = Routes[route].Depot; // depot
            node2 = Routes[route].Depot; // request or start of a break
            // distance from the depot to the fist node
            Travel = Travel + Cost[node1, node2];

            // the rest of the nodes in the tour    
            for (index = 1; index < Seq.Count-1; index++)
            {
                node1 = Seq[index - 1].From; // request origin node or break start
                node2 = Seq[index - 1].To; // request destination node
                node3 = index == Seq.Count -1? Routes[route].Depot : Seq[index].From; // next request origin node or break start or depot
                // time window violations
                Window += Math.Max(0, Pick[index - 1].Origin - Requests[node1].LatePickUp);
                Window += Math.Max(0, Pick[index - 1].Destin - Requests[node1].LateDelivery);

                // travel times
                Travel += Cost[node1, node2] + Cost[node2, node3];
            }

            // calculate shift violation
            Shift += Math.Max(0, Pick[Pick.Count - 1].Destin - Routes[route].FinishTime);            
        }
    }
}
