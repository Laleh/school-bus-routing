﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VRP
{
    public class clsInsertion
    {
        public void BestInsertion(List<sRoutes> Routes, List<sRequests> Requests, List<sNodes> Nodes, 
            int route, int node, double[,] Cost, double Clock,
            out List<sFromTo> BSeq, out List<sOrigDest> BPick, out double BTW, out double BShift, out Boolean InsFound)
            // find the best location to insert node into route and return the insertion cost
        {
            // classes to be used
            clsDelay solution = new clsDelay();

            // function variables
            List<sFromTo> TSeq = new List<sFromTo>(); // temp sequence  
            TSeq = Routes[route].seq.ToList();
            List<sOrigDest> TPick = new List<sOrigDest>(); // temp pic-kup
            TPick = Routes[route].pickup.ToList();
            List<sOrigDest> NewPick = new List<sOrigDest>(); // new pic-kup
            double TTW = 0, TShift = 0, TCost = 0; // insertion costs for sequence tardiness, shift violation, and node tardiness            
            int prenode, postnode;
            double Alpha = 0.5, Beta = 0.5; // parameters for TW and shift violations
            double BCost = double.PositiveInfinity;

            // function outputs
            BTW = 0;
            BShift = 0; // best cost values so far
            BSeq = TSeq.ToList();
            BPick = TPick.ToList();
            InsFound = false;

            // if sequence is empty
            if (TSeq.Count == 0)
            {
                BSeq.Add(new sFromTo(node, Requests[node].Destin));
                solution.Delay(Requests, Routes, Nodes, route, BSeq, Cost, out BPick, out BTW);
                BShift = Math.Max(0, BPick[BPick.Count - 1].Destin - Routes[route].FinishTime);
                InsFound = true;
                return;
            }

            // search through the route
            for (int index = 0; index < Routes[route].seq.Count-1; index++)
            {
                if (TPick[index].Origin > Clock)
                {
                    TShift = 0;
                    TTW = 0;
                    // the node after which "node" is to be inserted
                    prenode = Routes[route].seq[index].From;
                    postnode = Routes[route].seq[index + 1].From;                    
                
                    // find TW and shift violation imposed on the nodes in the sequence
                    // new sequence is passed over to Delay()
                    TSeq = Routes[route].seq.ToList(); // .ToList() is intended to prevent Routes[route].seq change when we change TSeq
                    TSeq.Insert(index + 1, new sFromTo(node, Requests[node].Destin));
                    solution.Delay(Requests, Routes, Nodes, route, TSeq, Cost, out NewPick, out TTW);
                    TShift = Math.Max(0, NewPick[NewPick.Count - 1].Destin - Routes[route].FinishTime);
                
                    // compare against the best solution so far
                    TCost = Alpha * TTW + Beta * TShift;
                    if (TCost < BCost)
                    {
                        BCost = TCost;
                        BTW = TTW;
                        BShift = TShift;
                        BSeq = TSeq.ToList();
                        BPick = NewPick.ToList();
                        InsFound = true;
                        if (TCost == 0)
                            return;
                    }
                }
            }
        }
    }
}
