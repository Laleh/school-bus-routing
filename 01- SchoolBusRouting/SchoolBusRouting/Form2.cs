﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VRP
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // call Main_heuristic
            Program S = new Program();

            List<sCostLog> CostLog;
            List<sNodes> Nodes;
            S.Main(out CostLog, out Nodes);

            // plot results
            for (int i = 0; i < CostLog.Count; i++)
            {
                chart1.Series["WaitTimes"].Points.AddXY(i, CostLog[i].WVal);
                chart1.Series["ShiftViolations"].Points.AddXY(i, CostLog[i].SVal);
                chart1.Series["BestSolution"].Points.AddXY(i, CostLog[i].Best);
                chart1.Series["Solution"].Points.AddXY(i, CostLog[i].Current);
            }
            //for (int i = 0; i < Nodes.Count; i++)
            //{
            //    chart2.Series["Truck Overtimes"].Points.AddXY(i, TruckCosts[i]);
            //}
            //Console.Read();
        }

        private void chart1_Click(object sender, EventArgs e) { }
        private void chart2_Click(object sender, EventArgs e) { }
    }
}
