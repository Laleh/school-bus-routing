﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;

namespace VRP
{
    public class clsLoadInput
        // read the input data
    {
        public void ReadInput(out List<sNodes> iNodes,out List<sRequests> iRequest, out List<sRoutes> iRoutes)
        {
            // new structures to be populated from the input file
            iRequest = new List<sRequests>(); 
            iNodes = new List<sNodes>();
            iRoutes = new List<sRoutes>();

            // pharse Json file for vehicles
            StreamReader re1 = new StreamReader("..\\..\\UnitsInput.txt");
            JsonTextReader reader1 = new JsonTextReader(re1);
            JsonSerializer test1 = new JsonSerializer();
            object mydata1 = test1.Deserialize(reader1);
            System.Web.Script.Serialization.JavaScriptSerializer ser1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            var nodes1 = ser1.Deserialize<List<sUnits>>(mydata1.ToString());

            for (int node = 0; node < nodes1.Count; node++) // check if the node is added already
            {
                Boolean Added = false;
                int DepotCode = -1;
                for (int index = 0; index < iNodes.Count; index++)
                {
                    if (Math.Abs(nodes1[node].Depot_x-iNodes[index].X) <= 0.001)
                    {
                        Added = true;
                        DepotCode = index;
                        break;
                    }
                }

                // add the depot node to the Nodes of the network
                if (!Added)
                {
                    iNodes.Add(new sNodes(nodes1[node].Depot_x, nodes1[node].Depot_y));
                    DepotCode = iNodes.Count - 1;
                }
                
                // add vehicle information
                iRoutes.Add(new sRoutes(nodes1[node].VehicleCode,nodes1[node].StartTime,
                    nodes1[node].FinishTime, nodes1[node].Depot_x, nodes1[node].Depot_y, DepotCode));
                iRoutes[iRoutes.Count - 1].seq = new List<sFromTo>();
                iRoutes[iRoutes.Count - 1].pickup = new List<sOrigDest>();
            }

            // pharse Json file for requests
            StreamReader re2 = new StreamReader("..\\..\\JsonInput.txt");
            JsonTextReader reader2 = new JsonTextReader(re2);
            JsonSerializer test2 = new JsonSerializer();
            object mydata2 = test2.Deserialize(reader2);
            System.Web.Script.Serialization.JavaScriptSerializer ser2 = new System.Web.Script.Serialization.JavaScriptSerializer();
            var nodes2 = ser2.Deserialize<List<sDemand>>(mydata2.ToString());
 
            // Requests and Nodes in the network           
            for (int index = 0; index < nodes2.Count; index++)
            {
                Boolean OAdded = false;
                Boolean DAdded = false;
                int OriginCode = -1;
                int DestinCode = -1;
                for (int node = 0; node < iNodes.Count; node++) // check if the node is added already
                {
                    if (Math.Abs(iNodes[node].X - nodes2[index].Origin_x) <= 0.001 && 
                        Math.Abs(iNodes[node].Y - nodes2[index].Origin_y) <= 0.001)
                    {
                        OAdded = true;
                        OriginCode = node;
                    }
                    if (Math.Abs(iNodes[node].X - nodes2[index].Destination_x) <= 0.001 && 
                        Math.Abs(iNodes[node].Y - nodes2[index].Destination_y) <= 0.001)
                    {
                        OAdded = true;
                        DestinCode = node;
                    }
                    if (OAdded && DAdded)
                        break;
                }

                if (!OAdded)
                {
                    iNodes.Add(new sNodes(nodes2[index].Origin_x, nodes2[index].Origin_y));
                    OriginCode = nodes2.Count - 1;
                }
                if (!DAdded)
                {
                    iNodes.Add(new sNodes(nodes2[index].Destination_x, nodes2[index].Destination_y));
                    DestinCode = nodes2.Count - 1;
                }

                // Add request
                iRequest.Add(new sRequests(nodes2[index].EarlyPickUp, nodes2[index].LatePickUp,
                    nodes2[index].EarlyDelivery, nodes2[index].LateDelivery));
                Boolean pause = false;
                if (DestinCode < 0)
                    pause = true;
                iRequest[iRequest.Count - 1].Origin = OriginCode;
                iRequest[iRequest.Count - 1].Destin = DestinCode;
            }
        }        
    }     
}
