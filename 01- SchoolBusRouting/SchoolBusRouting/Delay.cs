﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VRP
{
    public class clsDelay        
    {
        public void Delay(List<sRequests> Requests, List<sRoutes> Routes, List<sNodes> Centres, 
            int route, List<sFromTo> TSeq, double[,] Cost, out List<sOrigDest> NewPick, out double delay)
        // calculate the delay that would be imposed on a new sequence
        {
            // function variables
            List<sOrigDest> TPick = new List<sOrigDest>();
            int node1 = -1, node2 = -1, prenode;
            delay = 0;
            double arrival1 = 0, arrival2 = 0, length, travel = 0;
            int depot = Routes[route].Depot;
            // Main
            for (int index = 0; index < TSeq.Count; index++)
            {
                node1 = TSeq[index].From; // request
                node2 = TSeq[index].To; // centre 
                // previous centre/depot in the sequence (prenode)
                prenode = index == 0? Routes[route].Depot : TSeq[index-1].To;
                
                // arrival times at node1 and node2
                length = Cost[node1, node2];
                travel = Cost[prenode,node1];

                arrival1 = index == 0 ? Math.Max(Requests[node1].EarlyPickUp, Routes[route].StartTime + travel) : 
                    Math.Max(Requests[node1].EarlyPickUp, TPick[index - 1].Destin + travel);
                arrival2 = arrival1 + length;// +Requests[node1].Stime + Requests[node1].Ptime; 
                delay += Math.Max(0,arrival1-Requests[node1].LatePickUp);                            
                                
                TPick.Add(new sOrigDest(arrival1, arrival2));
            }           
            // function outputs            
            NewPick = TPick;
        }
    }
}
