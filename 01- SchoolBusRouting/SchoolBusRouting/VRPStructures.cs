﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Net;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;

namespace VRP
{
    // structire for Requests
    public class sRequests
    {
        public int Origin; // code for the origin node
        public int Destin; // code for the destination node
        public double EarlyPickUp; // start time of the pickup time window
        public double LatePickUp; // end time of the pickup time window
        public double EarlyDelivery; // start time of the pickup time window
        public double LateDelivery; // end time of the pickup time window
        public double ServiceTime;
        public double Call; // the time that the request is made

        public sRequests(double E1, double L1, double E2, double L2) // constructors
        {
            EarlyPickUp = E1;
            LatePickUp = L1;
            EarlyDelivery = E2;
            LateDelivery = L2;
        }
    }

    // structure for Nodes in the network, depots are included here as well
    public class sNodes
    {
        //public int Code;
        public double X;
        public double Y;

        public sNodes(double x, double y) // constructors
        {
            X = x;
            Y = y;
        }
    }

    public class sRoutes
    {
        public string VehicleCode;
        public double StartTime;
        public double FinishTime;
        public double Depot_x;
        public double Depot_y;
        public int Depot; // depot code
        public List<sFromTo> seq;
        public List<sOrigDest> pickup;      
        public double TVal; // Travel time value
        public double WVal; // Time winow violation
        public double SVal; // shift violation
        public double RVal; // The summation of used Rho matrix elements in the sequence

        public sRoutes(string ID, double start, double finish, double x, double y, int D)
        {
            StartTime = start;
            FinishTime = finish;
            Depot_x = x;
            Depot_y = y;
            VehicleCode = ID;
            Depot = D;
        }
    }
    
    public class sCodes // Will contain node structure to be used by LoadInput
    {
        public List<int> advance; // is 1 if a node is advance, 0 if it is centre, and -1 if it is emergent

        public sCodes(List<int> Advance)
        {
            advance = Advance;
        }
    }

    public class sRho
    {
        public List<int> Route;

        public sRho(List<int> R)
        {
            Route = R;
        }
    }

    public class sDemand
    {
        public double Origin_x { get; set; }
        public double Origin_y { get; set; }
        public double Destination_x { get; set; }
        public double Destination_y { get; set; }
        public double EarlyPickUp { get; set; }
        public double LatePickUp { get; set; }
        public double EarlyDelivery { get; set; }
        public double LateDelivery { get; set; }
    }

    public class sUnits
    {
        public string VehicleCode { get; set; }
        public double StartTime { get; set; }
        public double FinishTime { get; set; }
        public double Depot_x { get; set; }
        public double Depot_y { get; set; }
    }

    public class sCostLog
    {
        public double WVal;
        public double SVal;
        public double Best;
        public double Current;

        public sCostLog(double w, double s, double b, double c)
        {
            WVal = w;
            SVal = s;
            Best = b;
            Current = c;
        }
    }

    public class sFromTo
    {
        public int From;
        public int To;

        public sFromTo(int f, int t)
        {
            From = f;
            To = t;
        }
    }

    public class sOrigDest // arrival times at origin and destinations
    {
        public double Origin;
        public double Destin;

        public sOrigDest(double O, double D)
        {
            Origin = O;
            Destin = D;
        }    
    }

    public class sTabu // tabu list
    {
        public int Node;
        public int Route;

        public sTabu(int n, int r)
        {
            Node = n;
            Route = r;
        }
    }
}
