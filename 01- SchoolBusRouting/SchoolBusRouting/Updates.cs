﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VRP
{
    class clsUpdates
    {
        public void UpdatePick(List<sRequests> Requests, List<sRoutes> Routes, List<sNodes> Nodes,
            int route, List<sFromTo> TSeq, List<sOrigDest> TPick, double[,] Cost, int spot, 
            out List<sOrigDest> NewPick, out double Delay,
            out double Travel, out double Shift)
        // Given a new sequence, its corresponding pick-up is updated from location spot
        {
            // function variables
            int node1, node2, prenode, preCode = -1, index;
            Delay = 0;
            Travel = 0;
            Shift = 0;
            double arrival1, arrival2, length, travel = 0;            

            // Main
            NewPick = TPick.ToList();

            for (index = spot; index < TSeq.Count - 1; index++)
            {
                node1 = TSeq[index].From; // request
                node2 = TSeq[index].To; // centre
                // find the previous centre/depot in the sequence (prenode)
                prenode = TSeq[index - 1].To;                
                // arrival times at node1 and node2
                
                length = Cost[node1,node2];
                travel = Cost[preCode,node1];
                arrival1 = Math.Max(Requests[node1].EarlyPickUp, NewPick[index - 1].Origin + travel);
                Delay += Math.Max(0, arrival1 - Requests[node1].LatePickUp);
                Travel += travel + length;                      
                
                arrival2 = arrival1 + length + Requests[node1].ServiceTime; 
                NewPick.Add(new sOrigDest(arrival1, arrival2));
            }

            Shift = NewPick.Count > 0? Math.Max(0, NewPick[NewPick.Count - 1].Destin - Routes[route].FinishTime): 0;
        }
    }
}
